# Introducción

Explicación a la solución del punto 4 del tp2 del primer cuatrimestre del 2016 de
algorítmos y programación 2, cátedra Rosita, FIUBA.

# Dependencias:

* pdflatex
* minted
* tikz
* texlive-lang-spanish

# Como compilar.

``` bash
cd doc
make
```

